# Jenkins 安裝設定

  * [安裝](#架設Jenkins)
  * [設定流程](#設定Jenkins)
  * [管理Jenkins](#管理)
 
 
<a name="架設Jenkins"></a>
## 架設Jenkins

### 安裝
* 在安裝之前需要裝載homebrew (比較快)

1. 首先在安裝jenkins之前,jenkins是由java撰寫,所以先安裝java
	
		brew tap caskroom/versions
	
		brew cask install java8
> 注意!!!!!!!!! 這裡要確保是使用java8,否則會有異常現象發生,所以可以透過以上指令來指定安裝版本

2. 接著安裝jenkins
	
		brew update && brew install jenkins
	
3.	啟動jenkins
	
		brew services start jenkins
	
4.	安裝完後會有一組**預設密碼**,可透過以下指令來查看預設密碼

		more /Users/username/.jenkins/secrets/initialAdminPassword 
	
	> 如 : 07a27edb4d1c4b53b2598fde3adxxxXX

<a name="設定Jenkins"></a>
### 設定Jenkins


1.	先連上jenkins,使用以下IP,預設為8080
	
		 http://127.0.0.1:8080 

	> 預設為 8080 如要變更到 .jenkins/xxxxxxxxxxxx 
2.	進入設定畫面 , 這裡需要剛剛取得的預設密碼
	![Alt text](./resource/安裝預設密碼.png   "Optional title") 
3.	安裝套件
	![Alt text](./resource/安裝開始.png   "Optional title") 
4.	安裝成功後需要建立一組帳號
	![Alt text](./resource/建立帳號.png   "Optional title")  
5. 登入成功畫面
	![Alt text](./resource/建立新工作.png   "Optional title")
6. 擴充套件畫面
	![Alt text](./resource/模組安裝.png   "Optional title")  

#### jenkins 的強大之處就是有很多套件,並且也方便擴充
   
* 詳細這裡 
	* [https://plugins.jenkins.io/](https://plugins.jenkins.io/][https://plugins.jenkins.io/)

 
<a name="管理"></a>
## 管理Jenkins

升級
	
	brew jenkins upgrade

安裝目錄路徑

    /Users/username/.jenkins
 
刪除暫存路徑

    /Users/username/.jenkins/workspace


<a name="參考"></a>
##參考

[安裝 jenlins](http://flummox-engineering.blogspot.com/2016/01/installing-jenkins-os-x-homebrew.html)

