## Jenkins 應用與介紹

#### 簡介
* 	Jenkins是由Java撰寫的開源持續整合工具，可以幫助使用者達成專案建置、測試及部署等階段自動化的目標，是實現測試自動化及持續整合的利器
*  三竹資訊內部參考文件

<a name="mitake的CI,CD流程"></a>
## mitake的CI,CD流程
 
* mitake Jenkins工作流程,主要是透過shell script來完成編譯與發版,可簡化設定流程.

![Alt text](./jenkins_Image/流程.png   "Optional title")  

  * 參數化所有流程

  		/bin/sh -xe jenkins.sh MTK MTK InHouse "2V2W2494TP" "IMTK DEV IN HOUSE" "" "" "" "" "IMTK Today Widget DEV IN HOUSE"



#### 架設Jenkins相關
  * [安裝流程](./installation)
  * [Cluster](./cluster) - **加快運行速度**
 
#### 教學
  * [第一個IOS測試專案](./firstProject)
  * [整合Fastlane精簡編譯指令](./fastlane) 

### Unit Test 相關
* [使用ocunit2junit產出可視化報表](./ocunit2junit)
* [Covereage 報表]()
	
### Tirgger (觸發器)
* [daily trigger]() - 每日排程
* [webwook](./webhook) - gitlab 綁定觸發設定 


### Post (通知)
* [Slack]() - slack 串接
* [Mail通知]()

### 其他相關
* [與gitlab比較]()




