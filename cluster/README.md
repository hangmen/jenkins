## Cluster

### 目錄
* [簡介](#info)
* [設定slave](#info)
* [設定slave](#setup)
* [手動管理 Provision Profile and Keychanin](#keychain)
* [自動管理 Provision Profile and Keychanin](#auto_keychain)
* [啟用Slave](#start)

  		
<a name="info"></a>
### 簡介
* Cluster 顧名思義就是將主要主機當作是master,透過其他主機擔任slave角色,當master主機忙碌的時候將工作分配給slave執行


![Alt text](./resource/jenkins-multitier-topology.png   "Optional title") 

<a name="setup"></a>
### 設定slave

* 首先master需要取得slave的登入許可,這裡採用***SSH***的方式來登入slave主機
    
1. 設定SSH key

	* 先登入master
	* 然後透過以下指令來將publicKey放在slave主機上				
   		 	
   		 	ssh-copy-id -i ~/.ssh/id_rsa.pub yourUserName@yourIp
   			
    	> yourUserName : 要登入的使用者
    	
    	> yourIp : slave主機IP
	 
	![Alt text](./resource/設定SSH給奴隸.png   "Optional title") 
2. 登入master jenkins上選擇 Manage Jenkins -> Manage Nodes -> New Node 
 ![Alt text](./resource/slave0.png   "Optional title") 
 * 設定一些細相

	```
	HOST: 主機位址 ex: 127.0.0.1	
	Usage: 這裡有多種模式可選擇 , 這裡先選only build jobs label expressions this node	
	Launch Method: 選擇SSH
	```
	 ![Alt text](./resource/slave1.png   "Optional title")  
	  
3. 接下來如果連線成功會在Console看到以下狀態
  
  ![Alt text](./resource/Log.png   "Optional title") 


4. (可選)Slave建立Gitlab SSH存取(for cocoaPods)


<a name="keychain"></a>
### 手動管理 Provision Profile and Keychain

* slave與master有個最大問題,就是需要同步Provising Profile與keyChain,如果沒有處理好那編譯將會失敗.

1. 在Node profile設定Provising Profile存取路徑
	> /Users/{youruser}/Library/MobileDevice/Provising\ Profile

	![Alt text](./resource/設定NodePF路徑.png   "Optional title") 
*  手動Copy keychain and ProvisionProfile
	2. keychain
   
   a. 這裡先從master複製一份到slave
   > $HOME/Library/Keychains/login-keychain.db
   
   b. 上傳到slave的路徑
   > 預設 "$HOME/LoaclKeychain/mitake.keychain"
   
   c. 然後手動加到keychains程式裡面
   
   d. 再透過以下script來判斷是否需要解鎖slave的keychain

	    #預設的slave keychain存放路徑
		KEYCHAIN_PATH=$HOME/LoaclKeychain/mitake.keychain
		
		echo ${KEYCHAIN_PATH}
		if [ -f ${KEYCHAIN_PATH} ]
		then
		    echo "keychain pw :  $KEY_CHAIN_PASSWORD"
		    security unlock-keychain -p $KEY_CHAIN_PASSWORD ${keychain} &>/dev/null
		
		    if [ $? -ne 0 ];then
		           echo "Cannot open keychain ${keychain}"
		           exit 1
		    fi
		
		    echo "unlock OK"
		fi
 
	2. Provisioning Profiles
    
    a. 從master電腦複製一份到slave電腦
 > 預設存放路徑 $HOME/Library/MobileDevice/Provisioning\ Profiles/
 
     	scp $HOME/Library/MobileDevice/Provisioning\ Profiles/* mitake@10.1.4.154:/Users/mitake/Library/MobileDevice/Provisioning\ Profiles/   

<a name="auto_keychain"></a>
### 自動管理 ProfisionProfile and Keychanin

	***未完待續***

<a name="start"></a>
### 如何啟用Slave

1. 先點選專案然後選擇 Configure
2. 專案底下強制指定使用slave編譯
![Alt text](./resource/startupSlave1.png   "Optional title")
				
<a name="參考"></a>
###參考


[jenkins slave setup](https://blog.samsao.co/how-to-setup-a-jenkins-slave-running-mac-os-x-for-ios-projects-part-1-2937502ce90b)


[jenkins ssk error](https://stackoverflow.com/questions/41734737/why-jenkins-says-server-rejected-the-1-private-keys-while-launching-the-agen)

