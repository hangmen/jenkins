## 第一個IOS測試專案


<a name="測試流程"></a>
## 撰寫專案測試

### 所需PlugIn**
* Xcode integration

### 開啟新專案

1. 點選建立新工作
![Alt text](./resource/建立新工作.png   "Optional title")
2. 選取FreeStyle軟體專案
![Alt text](./resource/開新專案.png   "Optional title") 
3. 輸入程式碼來源
  * a
![Alt text](./resource/選取來源.png   "Optional title") 

![Alt text](./resource/編譯成功.png   "Optional title") 

	
* Configure XCode Plugin.
 
1. 這裡必需設定**Configuration**為**Dubug**
![Alt text](./resource/單元測試設定1.png   "Optional title") 

2. Code signing & OS X keychain options 裡面填入 **Team ID**
![Alt text](./resource/單元測試設定2.png   "Optional title") 
3. Advanced Xcode build options 這裡需做些設定
	* Xcode Schema File => 這裡為專案的schema為Hello
	* Custom xcodebuild arguments => 這裡需加入的參數
	> test -destination 'platform=iOS Simulator,name=iPhone 6,OS=11.0'
	* Xcode Project Directory => 專案目錄 ./Hello/Hello
	  
	![Alt text](./resource/單元測試設定3.png   "Optional title") 

### 啟動測試 
 * 然後就可以跑起來測試了

 
### 使用 fastlane scan 來測試 (alterntive)

1. 先安裝 xcode 工具組 

		xcode-select --install
2. 安裝 fastlane
	
		brew cask install fastlane
3. 設定fastlane環境變數
	
	fastlane requires some environment variables set up to run correctly. In particular, having your locale not set to a UTF-8 locale will cause issues with building and uploading your build. In your shell profile add the following lines:
	
		export LC_ALL=en_US.UTF-8
		export LANG=en_US.UTF-8
	
	You can find your shell profile at ~/.bashrc, ~/.bash_profile or ~/.zshrc depending on your system.

4. 專案中新增一段script,並按下儲存

		export PATH="$HOME/.fastlane/bin:$PATH"
		
		cd Hello/Hello
		
		fastlane scan -s Hello

	![Alt text](./resource/設定fastlaneScan.png   "Optional title") 
	
	
	
4. 測試成功	
![Alt text](./resource/fastlane測試成功.png  "Optional title") 
	
##參考

[升級到 9.0 打包失敗](http://blog.csdn.net/kongdeqin/article/details/78050599)

[cocoaPod 問題一些設定問題](http://www.jianshu.com/p/969dcb9907cf)

[jenkins unit test教學](https://medium.com/@jerrywang0420/unit-test-ui-test-jenkins-c-i-教學-ios-800cab673b6d)
