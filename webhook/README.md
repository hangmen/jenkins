<a name="Webhook"></a>
## Webhook

* **Webhook 是讓一個網站能訂閱另一個網站的方法**
  		

### 為何需要Webhook ?
* 當有版本控制網站一有Push或是Merge事件觸發,可透過webhook來啟動jenkins上的任務,而達到自動化的功用
	* 通常jenkins一被叫醒就會做下面的事 
		 1. 跑起單元測試來判斷此版本是否有問題
			* 可以一併統計覆蓋率
		 2. 自動封裝測試版本
		 	* 可以馬上包出一個測試版供給測試人員使用

### 使用GitLab綁定jenkins
* gitlab 當有push or merge可以透過webhook來啟動jenkins任務,而jenkins可以達成的目標就是 :
 1. unit test or integration test (測試)
 2. build test (看是否可以編譯成功)
 3. automatic deployment 自動包板

### Webhook設定流程
### jenkins上設定

1. 先到jenkins裡面開啟專案設定

* 開啟專案 -> 設定專案 -> Tigger 事件點選 build when a change is pushed to GitLab  ...

![](./resource/jenkins_setup.png)

* 這裏有兩點事要做
 1. 記下URL "http://10.1.4.139:8888/xxxx//xx"等下gitlab設定要用
 2. 產生Secret token,產生一組Secret token等下gitlab設定要用


### 設定GitLab

![](./resource/gitlab_setup.png)

* 接下來去你要設定的專案 (注意你要是專案owner才有設定選項)
* 開啟 Setting -> integration
* 填入剛剛的 URL
* 填入剛剛的 Secret token
* 然後就可以測試了

### 測試
* 當每次push的時候就可以呼叫webhook,我們只需要確認是否有正確執行
* 開啟 Setting -> integration 點選webhooks 底下你的webhook 點編輯

![](./resource/gitlab_test.png)

* 然後你就可以看你的最近 Deliveries 是否有推成功

![](./resource/gitlab_test1.png)


###參考

[ Gitlab WEBHOOK on jenkins
] (https://github.com/elvanja/jenkins-gitlab-hook-plugin#parameterized-projects
)
