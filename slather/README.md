## Slather Coverage

###  Generate test coverage reports for Xcode projects & hook it into CI.

| 需要套件 |
| --- |
| ruby |
| slather | 

1. 首先下載 slather
    1. gem install slather
2. 測試你的專案是否有開啟coverage 下圖

￼

3. 結果出爐

￼

4. 可以產生網頁



## 參考
[https://github.com/SlatherOrg/slather](https://github.com/SlatherOrg/slather)
