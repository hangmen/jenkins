## ocunit2junit 與 Jenkins 整合

| 需要套件 |
| --- |
| ruby |
| ocunit2junit | 

1. 先安裝ocunit2junit
	
	unix 系統直接下以下指令即可 , 確認是否有先安裝ruby
	
	```
		gem install ocunit2junit
	```
2. 先在本地端測試ocunit2junit

	1. 使用Hello專案來做測試	
	```
		xcodebuild test -target Hello -scheme Hello 
		-destination 'platform=iOS Simulator,name=iPhone 6s' 
		-configuration Debug -	enableCodeCoverage YES 2>&1 | ocunit2junit
	```

3. 成功會出現test-reports資料夾
 	![Alt text](./resource/1.jpg "aa")
 	> 含有測試報表

4. 之後前往jenkins設定專案
	
	* 開啟Hello專案
	1. 加入一個**執行Shell** , 並且輸入剛剛產生的command
		![Alt text](./resource/2.jpg "aa")
￼
	2. 接下來在**新增建置後動作**選擇**發布JUnit測試報告結果**
		*	輸入目錄 **test-reports/\*.xml**
		![Alt text](./resource/3.jpg "aa")
	3. 然後執行建置專案
￼
5. 完成測試結果趨勢圖出爐
￼	![Alt text](./resource/4.jpg "aa")
	> 每次的測試結果將會被記錄
	
	> 會跟之前的測試結果做一個比較表 , 以利方便省查

