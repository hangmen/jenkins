
 
### 使用 fastlane scan 來測試 (alterntive)

1. 先安裝 xcode 工具組 

		xcode-select --install
2. 安裝 fastlane
	
		brew cask install fastlane
3. 設定fastlane環境變數
	
	fastlane requires some environment variables set up to run correctly. In particular, having your locale not set to a UTF-8 locale will cause issues with building and uploading your build. In your shell profile add the following lines:
	
		export LC_ALL=en_US.UTF-8
		export LANG=en_US.UTF-8
	
	You can find your shell profile at ~/.bashrc, ~/.bash_profile or ~/.zshrc depending on your system.

4. 專案中新增一段script,並按下儲存

		export PATH="$HOME/.fastlane/bin:$PATH"
		
		cd Hello/Hello
		
		fastlane scan -s Hello

	![Alt text](./resource/設定fastlaneScan.png   "Optional title") 
	
	
	
4. 測試成功	
![Alt text](./resource/fastlane測試成功.png  "Optional title") 
	

## Fastlane 
* astlane action /// 查詢build-in元件


* fastlane cert 

		fastlane sigh --team_id 4DQLE3NABV --app_identifier com.a-mtk.testapp --username amtkApp@gmail.com
		
* 增加一個internet-password
		
		security add-internet-password -a "amtkApp@gmail.com" -w "xxxxxxxxx" -s "deliver.amtkApp@gmail.com"


### 比較 fastlane , xcodebuild , jenkins plugin

| | fastlane | xcodebuild | jenkins plugin |
| --- | --- | --- | --- |
| 複雜度 |  低   | 高   |  普通   |
| 延展性 |  低   | 高  | 低  |
| 學習曲線 |  低 | 高   | 低    |

>以複雜度來fastlane最低,它已經把大量的流程簡化,有時只需要一兩行即可實現,但相對彈性較低,而xcodebuild則是學習曲線較高但延展性最高,而jenkins plugin適合輕量化的編譯,太複雜可能還是採用前兩種方法來實現



